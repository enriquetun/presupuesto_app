import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceroComponent } from './shared/cabecero/cabecero.component';
import { EgresoComponent } from './shared/egreso/egreso.component';
import { IngresoComponent } from './shared/ingreso/ingreso.component';
import { FormularioComponent } from './shared/formulario/formulario.component';
import { IngresoService } from './services/ingreso.service';
import { EgresoService } from './services/egreso.service';

@NgModule({
  declarations: [
    AppComponent,
    CabeceroComponent,
    EgresoComponent,
    IngresoComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    IngresoService,
    EgresoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
