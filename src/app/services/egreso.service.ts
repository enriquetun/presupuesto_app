import { Injectable } from '@angular/core';
import { Egreso } from '../model/egreso.model';

@Injectable({
  providedIn: 'root'
})
export class EgresoService {
  egresos: Egreso[] = [
    new Egreso('Renta departamento', 2000),
    new Egreso('Ropa', 2000)
  ];

  eliminar(egreso: Egreso) {
    const indice: number =  this.egresos.indexOf(egreso);
    this.egresos.splice(indice, 1);
  }
  constructor() { }
}
