import { Component } from '@angular/core';
import { Ingreso } from './model/ingreso.model';
import { Egreso } from './model/egreso.model';
import { IngresoService } from './services/ingreso.service';
import { EgresoService } from './services/egreso.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ingresos: Ingreso[] = [];
  egresos: Egreso[] = [];

  constructor(private ingresoService: IngresoService, private egresoService: EgresoService) {
    this.ingresos = ingresoService.ingreso;
    this.egresos = egresoService.egresos;
  }

  getIngresoTotal() {
// tslint:disable-next-line: no-inferrable-types
    let ingresoTotal: number = 0;
    this.ingresos.forEach(ingreso => {
      ingresoTotal +=  ingreso.valor;
    });
    return ingresoTotal;
  }

  getEgresoTotal() {
// tslint:disable-next-line: no-inferrable-types
    let egresoTotal: number = 0;
    this.egresos.forEach(egreso => {
      egresoTotal += egreso.valor;
    });
    return egresoTotal;
  }
  getPorcentajeTotal() {
    return this.getEgresoTotal() / this.getIngresoTotal();
  }

  getPresupuestoTotal() {
    return this.getIngresoTotal() - this.getEgresoTotal();
  }
}
