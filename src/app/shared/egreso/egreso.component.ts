import { Component, OnInit, Input } from '@angular/core';
import { Egreso } from '../../model/egreso.model';
import { EgresoService } from '../../services/egreso.service';

@Component({
  selector: 'app-egreso',
  templateUrl: './egreso.component.html',
  styleUrls: ['./egreso.component.css']
})
export class EgresoComponent implements OnInit {

  egresos: Egreso[] = [];
  @Input() ingresoTotal: number;

  constructor(private egresoService: EgresoService) { }

  ngOnInit() {
    this.egresos =  this.egresoService.egresos;
  }

  eliminarEgresos(egreso: Egreso) {
    this.egresoService.eliminar(egreso);
  }

  calcularPorcentaje(egreso: Egreso) {
    return egreso.valor / this.ingresoTotal;
  }

}
