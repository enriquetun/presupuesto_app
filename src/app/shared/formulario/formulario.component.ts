import { Component, OnInit } from '@angular/core';
import { IngresoService } from '../../services/ingreso.service';
import { EgresoService } from '../../services/egreso.service';
import { Ingreso } from '../../model/ingreso.model';
import { Egreso } from '../../model/egreso.model';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

// tslint:disable-next-line: no-inferrable-types
  tipo: string = 'ingresoOperacion';
  descripcionInput: string;
  valorInput: number;

  constructor( private ingresoServicio: IngresoService, private egresoService: EgresoService) { }

  ngOnInit() {
  }
  tipoOperacion(evento) {
    this.tipo = evento.target.value;
  }
  agregarValor() {
     if (this.tipo === 'ingresoOperacion') {
       this.ingresoServicio.ingreso.push(new Ingreso(this.descripcionInput, this.valorInput));
     } else {
       this.egresoService.egresos.push(new Egreso(this.descripcionInput, this.valorInput));
     }
  }
}
